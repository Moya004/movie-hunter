﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using odev3.Models;
using System.Data.Entity;

namespace odev3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Movie_ReviewEntities db = new Movie_ReviewEntities();
            var movies = db.Movies_tbl.ToList();
            return View(movies);
        }
        public ActionResult Search(string name)
        {
            Movie_ReviewEntities db = new Movie_ReviewEntities();
            var movies = db.Movies_tbl.Select(x=>x.MovieName).ToList();
            var movie = movies.Contains(name);
            return View(movie);
        }
        public ActionResult _News()
        {
            Movie_ReviewEntities db = new Movie_ReviewEntities();
            var movies = db.News_tbl.ToList();
            return View(movies);
        }
        public ActionResult _Rating(int? id)
        {
            Movie_ReviewEntities db = new Movie_ReviewEntities();
            Movies_tbl movie = db.Movies_tbl.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }
        public ActionResult _AddRating(int id)
        {
            Movie_ReviewEntities db = new Movie_ReviewEntities();
            List<Rating_tbl> ratings = db.Rating_tbl.Where(x => x.MovieID == id).ToList() ;
            var rating = new Rating_tbl();
            if (ratings.Count != 0)
            {
                rating = ratings.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            }
            if (rating == null || ratings.Count==0){
                rating = new Rating_tbl();
                rating.MovieID = id;
                rating.Rating = -1;
                    };
            return View(rating);
        }

        // GET: Movie_tbl/Edit/5
        public ActionResult About(int? id)
        {
            Movie_ReviewEntities db = new Movie_ReviewEntities();
            Movies_tbl movie = db.Movies_tbl.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }
        [HttpPost]
        public ActionResult About(Rating_tbl rating_tbl)
        {
            if (ModelState.IsValid)
            {
                Movie_ReviewEntities db = new Movie_ReviewEntities();
                db.Rating_tbl.Add(rating_tbl);
                db.SaveChanges();
                return RedirectToAction("About","Home",new {id = rating_tbl.MovieID});
            }
            return View(rating_tbl);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}