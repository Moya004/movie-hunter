﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace odev3.Areas.Admin.Models
{
    public class MovieModel
    {
        public string MovieName { get; set; }
        public System.DateTime ReleaseDate { get; set; }
        public string MovieGenre { get; set; }
        public HttpPostedFileBase MoviePoster { get; set; }
        public int Duration { get; set; }
        public string Director { get; set; }
        public string Writer { get; set; }
        public string Trailer { get; set; }
        public string Country { get; set; }
        public Nullable<double> Rating { get; set; }
    }
}